# -*- coding: utf-8 -*-

# Copyright (c) Huoty, All rights reserved
# Author: Huoty <sudohuoty@163.com>
# CreateTime: 2025-02-25 10:31:20

def _import_module(name):
    try:
        return __import__(name)
    except ImportError:
        return None


jqdatasdk = _import_module("jqdatasdk")
jqdatahttp = _import_module("jqdatahttp")


if jqdatasdk:
    get_trade_days = jqdatasdk.get_trade_days
    get_all_securities = jqdatasdk.get_all_securities2
    get_security_info = jqdatasdk.get_security_info2
    get_price = jqdatasdk.get_price
    get_bars = jqdatasdk.get_bars
    get_ticks = jqdatasdk.get_ticks
